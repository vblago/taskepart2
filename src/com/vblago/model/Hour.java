package com.vblago.model;

public class Hour extends Workers {
    private float hourSalary;
    private float hours;
    private final float COURSE_USD = 27.25f;

    public Hour(String fullName, String post, float hourSalary, float hours) {
        this.fullName = fullName;
        this.post = post;
        this.hourSalary = hourSalary;
        this.hours = hours;
    }

    public float[] payHour() {
        float salary[] = new float[2];
        salary[0] = this.hourSalary * this.hours*COURSE_USD;
        salary[1] = salary[0] * 12*COURSE_USD;
        return salary;
    }
}
