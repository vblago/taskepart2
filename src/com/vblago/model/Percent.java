package com.vblago.model;

public class Percent extends Workers {
    private float percentSalary;
    private float amount;

    public Percent(String fullName, String post, float percentSalary, float amount) {
        this.fullName = fullName;
        this.post = post;
        this.percentSalary = percentSalary;
        this.amount = amount;
    }

    public float[] payPercent() {
        float salary[] = new float[2];
        salary[0] = this.amount / this.percentSalary / 12;
        salary[1] = salary[0] * 12;
        return salary;
    }
}
