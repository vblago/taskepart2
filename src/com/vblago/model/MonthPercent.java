package com.vblago.model;

public class MonthPercent extends Workers {
    private float percentSalary;
    private float monthSalary;
    private float amount;

    public MonthPercent(String fullName, String post, float monthSalary, float percentSalary, float amount) {
        this.fullName = fullName;
        this.post = post;
        this.percentSalary = percentSalary;
        this.monthSalary = monthSalary;
        this.amount = amount;
    }

    public float[] payMonthPercent() {
        float salary[] = new float[2];
        salary[0] = this.monthSalary + this.amount / this.percentSalary / 12;
        salary[1] = salary[0] * 12;
        return salary;
    }
}
