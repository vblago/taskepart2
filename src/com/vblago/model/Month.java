package com.vblago.model;

public class Month extends Workers {
    private float monthSalary;

    public Month(String fullName, String post, float monthSalary) {
        this.fullName = fullName;
        this.post = post;
        this.monthSalary = monthSalary;
    }

    public float[] payMonth() {
        float salary[] = new float[2];
        salary[0] = this.monthSalary;
        salary[1] = salary[0] * 12;
        return salary;
    }
}
