package com.vblago.ui;

import com.vblago.util.Generator;
import com.vblago.util.Pay;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        float[] salary;
        Pay pay = new Pay(Generator.generateWorkers());
        System.out.println("Input worker: ");
        String workerInp = in.nextLine();
        salary = pay.paySalary(workerInp);
        if (salary != null){
            System.out.println(String.format("Salary per month: %.2f" , salary[0]));
            System.out.println(String.format("Salary per year: %.2f" , salary[1]));
        }else {
            System.out.println("Cant`t find this worker");
        }
    }
}
