package com.vblago.util;

import com.vblago.model.*;


public class Pay {
    private static Workers[] workers;

    public Pay(Workers[] workers) {
        Pay.workers = workers;
    }

    public float[] paySalary(String workerInp) {
        float salary[];
        Workers worker = findWorkerByName(workerInp);
        if (worker == null) {
            return null;
        }
        salary = getOwnSalary(worker);

        return salary;
    }

    private Workers findWorkerByName(String workerInp) {
        Workers worker = null;
        for (Workers workerByName : workers) {
            if (workerByName.isNameOfWorker(workerInp)) {
                worker = workerByName;
                break;
            }
        }
        return worker;
    }

    private float[] getOwnSalary(Workers worker) {
        float[] salary = new float[2];
        if (worker instanceof Month) {
            salary = ((Month) worker).payMonth();
        } else if (worker instanceof Hour) {
            salary = ((Hour) worker).payHour();
        } else if (worker instanceof Percent) {
            salary = ((Percent) worker).payPercent();
        } else if (worker instanceof MonthPercent) {
            salary = ((MonthPercent) worker).payMonthPercent();
        }
        return salary;
    }

}
