package com.vblago.util;

import com.vblago.model.*;

public class Generator {
    //почему класс нельзя сделать static?

    public static Workers[] generateWorkers(){
        Workers[] workers = new Workers[4];
        workers[0] = new Month("Иванова Елена Львовна", "зам директора", 4500);
        workers[1] = new Hour("Вакуленко Дмитрий Владимирович", "дизайнер", 7, 130);
        workers[2] = new Percent("Коренькова Анна Павловна", "менеджер по продажам", 5, 115000);
        workers[3] = new MonthPercent("Коренькова Татьяна Сергеевна", "менеджер по продажам", 1000, 5, 115000);
        return workers;
    }
}
